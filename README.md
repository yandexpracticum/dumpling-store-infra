# Dumplings Store Infra

## Требуемое окружение

Для развертывания инфраструктуры было необходимо:
1. Устновить следущие утилиты:
- yc (https://cloud.yandex.ru/docs/cli/quickstart);
- Terraform (https://learn.hashicorp.com/tutorials/terraform/install-cli);
- Kubectl (https://kubernetes.io/docs/tasks/tools/install-kubectl/);
- git;
- ssh.
- helm
2. Зарегистрироваться в Яндекс.Облаке (https://cloud.yandex.ru/);
3. Добавить в облако каталог, в котором будут создаваться ресурсы.

## Подготовка окружения
### Вход в облако

Данные шаги можно было выполнить силами terrafrom, но было решено проделать их заранее:
- Проверяем предсозданные ресурсы в облаке (сеть и подсети)
- Создаем сервисные аккаунты для k8s с ролью editor и image.puller

### Настройка консольной утилиты yc для работы с Яндекс Облаком

Документация: https://cloud.yandex.ru/docs/cli/quickstart

### Развертывание кластера с помощью Terraform

- Подключения к провайдеру и создание кластера описано в main.tf
- Сохранения состояния в бакете S3 прописано в main.tf
- Переменные хранятся в terraform.tfvars (.gitignore)

1. Заполнить файл config.s3.tfbackend содержимым ключей пользователя:

```
access_key = "xxx"
secret_key = "xxx"
```

2. Инициализация terraform:

 ```
terraform init
 ```

3. Создать ресуры terraform:

 ```
terraform apply
 ```

4. Добавьте в Helm репозиторий для NGINX:

```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
```

5. Обновите набор данных для создания экземпляра приложения в кластере Kubernetes:

```
helm repo update
```

6. Установите NGINX Ingress Controller:

```
helm install ingress-nginx ingress-nginx/ingress-nginx
```

7. Установите менеджер сертификатов:

```
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.1/cert-manager.yaml
```

8. Прописать А-запись своему домену на ип-ингресс. Посмотреть ip

```
kubectl get svc
```

### Разворачивание мониторинга


Создайте клон репозитория с Helm-чартами: 

`git clone https://gitlab.praktikum-services.ru/root/monitoring-tools`

Меняем ingress графаны на нужный нам хост и директории monitoring-tools установите Grafana с помощью Helm-чарта: 

`helm upgrade --atomic --install grafana grafana`

Устанавливаем prometheus меняя также хост:

`helm upgrade --atomic --install prometheus prometheus`

Устанавливаем локи:

```
helm repo add loki https://grafana.github.io/loki/charts
helm repo update
helm upgrade --install loki loki/loki-stack
```

Заходим в графану и добавляем datasource loki: http://loki:3100 



