terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"


# Описание бэкенда хранения состояния
backend "s3" {
  endpoint   = "storage.yandexcloud.net"
  bucket     = "terraform-state-fortysixth"
  region     = "ru-central1"
  key        = "terraform.tfstate"

  skip_region_validation      = true
  skip_credentials_validation = true
  }
}

provider "yandex" {
  token = var.token
  zone = var.zone
  cloud_id = var.cloud
  folder_id= var.folder
}

#k8s cluster
resource "yandex_kubernetes_cluster" "fortysixth-cluster" {
  network_id  = var.network
  folder_id   = var.folder
  cluster_ipv4_range = "10.11.0.0/16"
  service_ipv4_range = "10.12.0.0/16"

  master {
    version = "1.21"
    zonal {
      zone      = var.zone
      subnet_id = var.sub
    }

    public_ip = true

  }

  service_account_id      = var.service-editor
  node_service_account_id = var.service-puller

}
