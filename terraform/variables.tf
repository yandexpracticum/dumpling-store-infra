#auto-complete tfvars

variable "token" {}
variable "folder" {}
variable "cloud" {}
variable "zone" {}
variable "sub" {}
variable "image" {}
variable "service-editor" {}
variable "service-puller" {}
variable "network" {}
